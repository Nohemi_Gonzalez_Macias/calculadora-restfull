# Actividad: Tablas RESTfull

La aplicación web de este repositorio es una práctica de los métodos POST, GET, PUT y DELETE, con puso de parámetros por la URI o por el cuerpo de la petición.


# Tabla RESTfull 
Esta aplicación sigue lo especificado en la siguiente tabla RESTfull:

![Tabla RESTfull](public/images/calculadora_tablaRESTfull.png)

# Control de directorio padre
Además, se ha añadido un control sobre la url `/results/`, para que no dé errores.

![Comprobación de la ruta results sin prámetros](public/images/users_sin_parametros.png)

# Comprobación de l funcionamiento de la aplicación
Usted puede comprobar los métodos de esta aplicaición con el software *Postman*, que puede obtener del enlace:

[Enlace al sitio oficial de Postman](https://www.getpostman.com/)

