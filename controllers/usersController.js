const express = require('express'); // invocamos express

// Métodos del controlador por cada método de la tabla Restful
// El método recibe el request, response y el 'next'.
// Por cada método, su respuesta es un mensaje, en este caso, mensaje por cada tipo de método
function create(req, res, next){
  res.send(`Crea un usuario con nombre ${req.body.name}`);
};

function list(req, res, next){
  //res.send("Lista los usuarios");
  //let name = req.params.name ? req.params.name : 'Desconocido';
  //res.render('users/list', {name: name});
  res.render('users/list', {name: req.params.name});
};

function index(req, res, next){
  res.send(`Lista un usuario con un ${req.params.id} específico`);
};

function update(req, res, next){
  res.send("Edita un usuario con un id específico");
};

function destroy(req, res, next){
  res.send("Borra un usuario con un id específico");
};

// Para que el archivo de rutas pueda usar esas funciones sin problema
module.exports = {
  create,
  list,
  index,
  update,
  destroy
};
