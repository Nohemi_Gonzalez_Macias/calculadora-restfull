const express = require('express');

function sumar(req, res, next){
  let result = parseInt(req.params.n1) + parseInt(req.params.n2)
  res.render('results/sumar', {n1: req.params.n1, n2: req.params.n2, result: result});
  // let x = {'resultado:' parseInt(req.params.n1) + parseInt(req.params.n2)}
  // res.json({x});
}

function multiplicar(req, res, next){
  res.send(`Multiplica los números ${req.body.n1} y ${req.body.n2}. Su resultado es: ${parseInt(req.body.n1) * parseInt(req.body.n2)}`);
}

function dividir(req, res, next){
  res.send(`Divide los números ${req.body.n1} y ${req.body.n2}. Su resultado es: ${parseInt(req.body.n1) / parseInt(req.body.n2)}`);
}

function restar(req, res, next){
  res.send(`Resta los números ${req.params.n1} y ${req.params.n2}. Su resultado es: ${parseInt(req.params.n1) - parseInt(req.params.n2)}`);
}

module.exports = {
  sumar,
  multiplicar,
  dividir,
  restar
};
